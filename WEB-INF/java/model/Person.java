package model;

import annotation.Url;

import java.util.HashMap;

public class Person {
    private int id;
    private String nom;

    public Person() {}

    public Person(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Url(url = "list-emp")
    public ViewModel list_employe() {
        ViewModel model = new ViewModel();
        Person[] liste = new Person[3];
        liste[0] = new Person(1, "Naivo");
        liste[1] = new Person(2, "Koto");
        HashMap<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < liste.length; i++) {
            map.put("dept"+liste[i].getId(), liste[i]);
        }
        model.setData(map);
        model.setJsp("ListEmp.jsp");
        return  model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
