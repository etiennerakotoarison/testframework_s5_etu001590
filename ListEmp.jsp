<%@ page import="java.util.HashMap" %>
<%@ page import="model.Person" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste des personnes</title>
    <%
        HashMap<String, Object> data = (HashMap<String, Object>)request.getAttribute("data");
    %>
</head>
<body>
<table border="1" cellpadding="10" cellspacing="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    <%
        for (Object dept : data.values()) {
            Person d = (Person)dept;
            out.print("<tr>");
            out.print("<td>"+d.getId()+"</td>");
            out.print("<td>"+d.getNom()+"</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
